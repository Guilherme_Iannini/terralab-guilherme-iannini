# TerraLab -Guilherme Iannini



## <b> Sejam bem vindos!!
## <b> meu nome é Guilherme Iannini e Criei esse Repositório para participar do processo seletivo de Front end do Terralab.

## <i>  informações
1) email: guidutrav155@gmail.com
2) Formação: Graduado no Curso Técnico de Automação Industrial- IFMG-OP
   Formação: Cursando Engenharia de Controle e automação - UFOP

<b> Comprovantes Curso Git Udemy (Semana 1)
![udemy_1_semana](/uploads/a48ac895b4373d4017eeadabf8fcf5cd/udemy_1_semana.png)



<b> Comprovantes Curso Typescript Alura (Semana 2):

![Typescript_parte_1_alura](/uploads/bd5f511b7b96222cc4e6b5817ce3c843/Typescript_parte_1_alura.png)


![Typescript_parte_1_alura_2_](/uploads/4920daa11f21e5163da66ecbebe9f4fb/Typescript_parte_1_alura_2_.png)

![Typescript_parte_1_alura_3_](/uploads/1ff580e6d20b0df1ef99b0f9652207ce/Typescript_parte_1_alura_3_.png)
